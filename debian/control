Source: kaffeine
Section: video
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Mark Purcell <msp@debian.org>,
           Michael Meskes <meskes@debian.org>,
           Pino Toscano <pino@debian.org>,
Build-Depends: debhelper (>= 11~), cmake, pkg-kde-tools (>= 0.15.16),
 pkg-config,
 extra-cmake-modules (>= 1.0.0),
 qtbase5-dev (>= 5.4.0),
 libqt5x11extras5-dev (>= 5.4.0),
 libkf5coreaddons-dev (>= 5.11.0),
 libkf5doctools-dev (>= 5.41.0~),
 libkf5dbusaddons-dev (>= 5.11.0),
 libkf5i18n-dev (>= 5.11.0),
 libkf5kio-dev (>= 5.11.0),
 libkf5solid-dev (>= 5.11.0),
 libkf5widgetsaddons-dev (>= 5.11.0),
 libkf5windowsystem-dev (>= 5.11.0),
 libkf5xmlgui-dev (>= 5.11.0),
 libvlc-dev,
 libx11-dev, libxss-dev,
 libdvbv5-dev [linux-any], libudev-dev [linux-any],
 gettext,
Standards-Version: 4.4.0
Homepage: http://kaffeine.kde.org
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/kaffeine
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/kaffeine.git

Package: kaffeine
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 kinit,
 iso-codes,
 libqt5sql5-sqlite,
 vlc-plugin-base, vlc-plugin-video-output
Suggests: libdvdcss2
Description: versatile media player for KDE
 Kaffeine is a media player for KDE based on VLC, which gives it a wide variety
 of supported media types and lets Kaffeine access CDs, DVDs, and network
 streams easily.  Kaffeine also has an excellent support of digital TV (DVB).
 .
 Kaffeine can keep track of multiple playlists simultaneously, and supports
 autoloading of subtitle files for use while playing video.
